# FRDM-KL25Z

Network Embedded System (NES) and Embedded Operating System (EOS) project concerning FreeRTOS on a FREESCALE FREEDOM platform FRDM-KL25Z model.

## Usefull links

- [NXP official webpage](https://www.nxp.com);
- [NXP FRDM-KL25Z main webpage](https://www.nxp.com/design/development-boards/freedom-development-boards/mcu-boards/freedom-development-platform-for-kinetis-kl14-kl15-kl24-kl25-mcus:FRDM-KL25Z);
- [NXP FRDM-KL25Z get started guide](https://www.nxp.com/document/guide/get-started-with-the-frdm-kl25z:NGS-FRDM-KL25Z);
- [mbed FRDM-KL25Z overview](https://os.mbed.com/platforms/KL25Z/);
- [MCUXpresso SDK API reference](https://mcuxpresso.nxp.com/api_doc/dev/1364/);
- [MCUXpresso ISSDK API Reference Manual](https://mcuxpresso.nxp.com/api_doc/comp/194/index.html);
- [MCUXpresso SensorFusion API Reference Manual](https://mcuxpresso.nxp.com/api_doc/comp/196/);
- [PEmicro Development & Production Solutions - OpenSDA](http://www.pemicro.com/opensda/pe_tools.cfm);
- [PEmicro OpenSDA support](http://www.pemicro.com/opensda/);

# Quick start

## First connection

Connect the FRDM-KL25Z platform with a mini-USB cable to your PC and to the SDA port of the platform. You should see the FRDM-KL25Z platform as a normal device drive labelled *FRDM-KL25Z* connected to your PC operating system filesystem.

You can now open the device drive and see some files inside it:
- *FSL_WEB.HTM*: open the NXP web page of the FRDM-KL25Z platform;
- *SDA_INFO.HTM*: show some hardware information (**important**);
- *TOOLS.HTM*: link the resources page with all the firmware upgrade files;

Try to open the *SDA_INFO.HTM* file and see the hardware informations. By default you should be have 1.09 *Bootloader Version* and this is a great problem that will be treated in the next section. If you have the 1.11 *Bootloader Version* or more it's good. By default you should also have as *Installed Application* the PEMicro FRDM-KL25Z Mass Storage/Debug App, that can already used for MCUXpresso configuration, but if you want to use mbed OS, check for the [mbed firmware installation](https://os.mbed.com/platforms/KL25Z/).

# Developer guide

## Hardware info
- No support for ethernet, but can be plug in other board:
    - Arduino ethernet shield: [arduino-reference](https://www.arduino.cc/en/Main/ArduinoEthernetShieldV1);
    - ENC28J60 boards: [amazon-link](https://www.amazon.it/SunFounder-ENC28J60-Ethernet-Network-Arduino/dp/B00GAXAQOQ), [ebay-link](https://www.ebay.com/itm/New-ENC28J60-Ethernet-LAN-Network-Module-For-Arduino-SPI-AVR-PIC-LPC-STM32-/310670027142?pt=LH_DefaultDomain_0&hash=item4855606986);

## Bootloader upgrade

**Do not enter the bootloader mode if the platform bootloader version is 1.10 or less!!!** It is a great problem of some old FRDM-KL25Z platform, because they have installed by default the 1.09 bootloader version and with the most modern operating system if you try to enter in the bootloader mode you will erase all the memory of the firmware application code and you brick the platform. When this happened you should see *Application Version* 0.00 in *SDA_INFO.HTM* file.

The only operating system that can upgrade the bootloader platform when its version is 1.10 or less are Windows XP or Windows 7. With VM machines the upgrade steps don't work, therefore you have to follow the bootloader upgrade steps in a real machine with Windows XP or Widows 7 installed.

There is a trickness solution to upgrade the bootloader platform in Windows 8 or Windows 10 that is showed in [this tutorial](https://mcuoneclipse.com/2016/08/01/bricking_and_recovering_opensda_boards_in_windows_8_and_10/), but it's required a Windows Pro version with *Group Policy Editor* tool, because the solution with *Windows Registry* tool showed in the tutorial doesn't work. 

If the bootloader version is 1.11 or more, you should not have problem with the following bootloader upgrade steps.

Bootloader upgrade steps:
1. Download the Firmware Apps `.zip` file from the [OpenSDA Support page](http://www.pemicro.com/opensda/).
2. Extract the files inside the `.zip` file downloaded and then extract the *OpenSDA_Bootloader_Update_App_vXXX_XXXX_XX_XX.zip* file.
3. Connect the mini-USB cable while hold down the reset button, that is between the two USB ports, and then release it when you see the D4 led light on. Your board will then be visible as a drive labelled *BOOTLOADER*.
4. Drag the *BOOTUPDATEAPP_Pemicro_vXXX.SDA* file onto the *BOOTLOADER* device drive.
5. Disconnect and reconnect (**without** holding the reset button) the platform cable and check the *SDA_INFO.HTM* file for the succesfully upgrade.

Troubleshotting: if everything works correctly when your platform is in bootloader mode the led should blink 500ms ON and 500ms OFF and when your platform is in application mode should have the led always ON. If in bootloader mode the led is 2 seconds OFF followed by 8 rapid ON/OFF blinks you should follow the previous steps for bootloader upgrading, but if the led behaviour doesn't change, you could have the platform bricked and you have to upgrade the bootloader from a Windows XP or Windows 7 operating system as explained above. Otherwise I suggested to follow [this Windows 8 and Windows 10 tutorial](https://mcuoneclipse.com/2016/08/01/bricking_and_recovering_opensda_boards_in_windows_8_and_10/), but it's required a Windows Pro version with *Group Policy Editor* tool, because the solution with *Windows Registry* tool showed in the tutorial doesn't work. 

In particular if you have the platform behaviour similar to the one described in [this NXP community thread](https://community.nxp.com/thread/502888), you have to the platform bricked and you have to upgrade the bootloader from a Windows XP or Windows 7 operating system as explained above.

## Firmware upgrade
Check the section before and if you have an updated bootloader version (1.11 or more). Afterthat follow these steps to upgrade the firmware:
1. Download the Firmware Apps `.zip` file from the [OpenSDA Support page](http://www.pemicro.com/opensda/) and extract it or download any firmware upgrade S-record file (for example the one taken from the [mbed upgrade firmware](https://os.mbed.com/handbook/Firmware-FRDM-KL25Z) or others taken from [the latest NXP firmware application](https://www.nxp.com/design/microcontrollers-developer-resources/ides-for-kinetis-mcus/opensda-serial-and-debug-adapter:OPENSDA#nogo)).
2. Connect the mini-USB cable while hold down the reset button, that is between the two USB ports, and then release it when you see the D4 led light on. Your board will then be visible as a drive labelled *BOOTLOADER*.
3. Drag the *MSD-DEBUG-FRDM-KL25Z_Pemicro_vXXX* file or any S-record file onto the *BOOTLOADER* device drive.
4. Disconnect and reconnect (**without** holding the reset button) the platform cable and check the *SDA_INFO.HTM* file for the succesfully upgrade.

## MCUXpresso IDE and SDK setup
The suggested IDE by NXP for the FRDM-KL25Z is MCUXpresso IDE. The alternative is to refer to an [mbed](https://os.mbed.com/platforms/KL25Z/) solution. 

Follow these steps to configure the environment:
1. Download MCUXpresso IDE from this [ide-download-link](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE) and install with the downloaded installer;
2. Build and download the MCUXpresso SDK you are interested for (in our case ` MKL25Z128xxx4`) through the NXP dashboard from this [sdk-download-link](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools/mcuxpresso-software-development-kit-sdk:MCUXpresso-SDK);
    - Click on download button;
    - Click on *Select Development Board*;
    - Login with your NXP account or register;
    - Search by Name "FRDM-KL25Z" and select *FRDM-KL25Z (MKL25Z128xxx4)*;
    - Click on *Build MCUXpresso SDK*;
    - Wait that the build process finished and than you may be able to download the sdk;
3. To install the SDK, open the MCUXpressoIDE, launch a workspace, open the 
IDE view and click on the *Installed SDKs* tab in the button panel. Then drag 
and drop the SDK zip file you have downloaded in this *Installed SDKs* view.

## Initialize project with FreeRTOS
Check the [MCUXpresso IDE and SDK setup](#MCUXpresso\ IDE\ and\ SDK\ setup) section if you don't have your environment configured and then follow the steps below to create a new project with FreeRTOS:
1. Open *MCUXpresso IDE*, launch your workspace and go to IDE view.
2. From the top menu, click on *File* -> *New* -> *New C/C++ project*.
3. From the window that appears, you have to select your platform, so filter the available board for "FRDM-KL25Z" and select the first one appeared.
4. Click *Next* and configure the project name and the project location if you want to change the default one.
5. From the *Components* section, select the *Operating System* tab and select "freertos" instead of "baremetal".
6. Click *Finish* and your project is ready to be built and debugged.

## Debug firmware with OpenSDA
Check the *SDA_INFO.HTM* file for the platform firmware upgrade version and follow the steps in [Firmware upgrade](#Firmware\ upgrade) section if the following debug firmware steps do not work.

The OpenSDA driver tested are the version 1.18 of *PEMicro FRDM-KL25Z Mass Storage/Debug App*.

1. Connect the platform to your PC with a mini-USB cable.
2. Open *MCUXpresso IDE*, launch your workspace and go to IDE view.
3. From the top menu, click on *Run* -> *Debug Configurations...* -> *GDB PEMicro Interface Debugging* and create a new configuration with double click.
4. Click on *Debugger* tab and select from the *Inteface* menu the *OpenSDA Embedded Debug - USB Port*.
5. Now you should check that the *Port* selection automatically completes otherwise click on *Refresh*. If the *Port* selectiion is still empty, you should upgrade the right version of the OpenSDA firmware following the steps in [Firmware upgrade](#Firmware\ upgrade) section.
6. Click on *Apply* and *Debug* to start a new debug session.